package day2;

import com.dropbox.core.v2.DbxClientV2;

import javax.sound.sampled.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * Audio recording class for continuously recording sound from microphone and sending to Dropbox.
 */
public class JavaSoundRecorderVer2 {

    AudioFileFormat.Type fileType;
    TargetDataLine line;
    AudioFormat format;
    DbxClientV2 client;

    public JavaSoundRecorderVer2(DbxClientV2 client) {
        fileType = AudioFileFormat.Type.WAVE;
        float sampleRate = 16000;
        int sampleSizeInBits = 8;
        int channels = 2;
        boolean signed = true;
        boolean bigEndian = true;
        format = new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian);
        this.client = client;
    }

    public void recordAudio(int seconds) {
        String filePath = getCurrentTime() + ".wav";
        File file = new File("records/" + filePath);
        start(file);
        stop(file, seconds);
    }

    private void start(File file) {
        Thread thread = new Thread(() -> {
            try {
                DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
                line = (TargetDataLine) AudioSystem.getLine(info);
                line.open(format);
                line.start();

                AudioInputStream ais = new AudioInputStream(line);
                AudioSystem.write(ais, fileType, file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        thread.start();
    }

    private void stop(File file, int seconds) {
        Thread thread = new Thread(() -> {
            try {
                Thread.sleep(1000 * seconds);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            line.stop();
            line.close();

            // Uploading to Dropbox
            try {
                InputStream in = new FileInputStream(file);
                client.files()
                        .uploadBuilder("/" + file.getName())
                        .uploadAndFinish(in);
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!file.delete()) System.out.println(file + " couldn't be deleted");

            recordAudio(seconds);
        });

        thread.start();
    }

    private String getCurrentTime() {
        return new SimpleDateFormat("yyyyMMdd_hhmmss").format(new Date(System.currentTimeMillis()));
    }
}