package day2;

import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;

public class Main {

    public static void main(String[] args) {

        /*
         * I hide my dropbox token for security reasons. You can create your own, paste it here and test an app.
         * To create your own folder and token for it, go to Developer section of Dropbox and create a new app. There
         * you could generate an access token for your folder. Put it into ACCESS_TOKEN.
         */
        String ACCESS_TOKEN = "GENERATE AND PLACE YOUR OWN ACCESS TOKEN HERE";

        DbxRequestConfig config = DbxRequestConfig.newBuilder("dropbox/java-tutorial").build();
        DbxClientV2 client = new DbxClientV2(config, ACCESS_TOKEN);

        JavaSoundRecorderVer2 recorderVer2 = new JavaSoundRecorderVer2(client);

        recorderVer2.recordAudio(10);
    }
}
