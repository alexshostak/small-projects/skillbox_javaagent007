package day1;

public class CoffeeMachine
{
    private int[] drinkPrices;
    private String[] drinkNames;

    public CoffeeMachine(int[] prices, String[] names)
    {
        this.drinkPrices = prices;
        this.drinkNames = names;
    }

    public boolean checkPrices(int moneyAmount)
    {
        boolean canBuyAnything = false;
        for(int i = 0; i < drinkPrices.length; i++)
        {
            if(moneyAmount >= drinkPrices[i]) {
                System.out.println("Вы можете купить " + drinkNames[i]);
                canBuyAnything = true;
            }
        }
        return canBuyAnything;
    }
}