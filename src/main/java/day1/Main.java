package day1;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            System.out.println("Кофе-машина");

            //TODO: read from console !DONE
            System.out.print("Введите Ваши средства (ноль для выхода): ");
            int moneyAmount = scanner.nextInt();
            if (moneyAmount == 0) {
                System.out.println("Приходите еще :)");
                break;
            }

            int[] drinkPrices = {150, 80, 20};
            String[] drinkNames = {"капучино", "американо", "воду"};

            boolean can = new CoffeeMachine(drinkPrices, drinkNames).checkPrices(moneyAmount);
            if (!can) System.out.println("Недостаточно средств :( Изучайте Java и будьте всегда в достатке!))");


            //TODO: print current date and time: 20200924_144805 !DONE
            //SimpleDateFormat
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd_hhmmss"); //У меня 12-часовое время
            System.out.println(simpleDateFormat.format(new Date(System.currentTimeMillis())));
            System.out.println();
        }


        //TODO: create class day1.JavaSoundRecorder and change it to record
        // 30-second audio. Screenshot all codes and results and send
        // to Telegram-chat with hash-tag #уменяполучилось
        // https://www.codejava.net/coding/capture-and-record-sound-into-wav-file-with-java-sound-api
    }
}
