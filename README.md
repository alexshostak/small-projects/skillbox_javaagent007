# Skillbox_JavaAgent007

* Skillbox course:
    1. Create a program to record from Windows microphone and save a file on Desktop.
    2. Add methods to record audio continuously, send to Dropbox and then delete a file from local machine.

* In this program I worked with File and Thread classes, javax.sound.sampled package, Dropbox API and others. 